# median absolute pairwise difference
mapd <- function(x, ...) {
	median(abs(diff(x), ...), ...)
}

mapd_within_segment <- function(copy_ratio.df, segments.df) {
	y <- mapply(
		function(chrom, start, end) {
			idx <- copy_ratio.df$contig == chrom;
			copy_ratio.sub <- copy_ratio.df[idx, ];
			diff(copy_ratio.sub$VALUE[
				copy_ratio.sub$start >= start & copy_ratio.sub$stop <= end
			])
		},
		segments.df$Chromosome,
		segments.df$Start,
		segments.df$End
	)

	median(abs(unlist(y)))
}

waviness <- function(chromosomes, positions, values, window.size=1e6, min.markers=10) {
	abs.devs <- rep(NA, length(values));

	chrom.levels <- unique(chromosomes);
	for (chrom in chrom.levels) {
		idx <- chromosomes == chrom;
		n <- length(idx);
		positions.sub <- positions[idx];
		values.sub <- values[idx];
		max.position <- positions.sub[length(positions.sub)];

		window.ends <- seq(window.size, max.position, window.size);
		i <- 1;
		for (window.end in window.ends) {
			# search for end of window
			j <- i;
			while (j < n && positions.sub[j] <= window.end) {
				j <- j + 1;
			}
			# compute absolute deviation from median within window
			if (j - i + 1 > min.markers) {
				x <- values.sub[i:j];
				abs.devs[idx][i:j] <- abs(x - median(x));
			}
			# advance to next marker
			i <- j + 1;
		}
	}
	median(abs.devs, na.rm=TRUE)
}
