#include "gc_content.hpp"

namespace gwalker {

bool is_nucleotide(char x) {
	const char nucleotides[] = "ACGTRYSWKMBDHVN";
	return (strchr(nucleotides, x) != NULL);
}

double get_gc_content_of_window(const vector<char>& window) {
	const char nucleotides[] = "GCS";

	size_t x = 0, n = 0;

	for (size_t j = 0; j < window.size(); ++j) {
		char w = window[j];
		if (w != 'N') {
			if (strchr(nucleotides, w) != NULL) {
				// nucleotide is a G or C
				++x;
			}
			++n;
		}
	}

	return double(x) / n;
}

void scan_gc_content(
	const string& fasta_fname, long int window_size, long int step_size,
	vector<genomic_track>& tracks
) {

	if (window_size < 0) {
		throw runtime_error("Window size must be positive");
	}

	if (step_size < 0) {
		throw runtime_error("Step size must be positive");
	}

	if (step_size > window_size) {
		throw runtime_error("Step size cannot be greater than window size");
	}

	ifstream inf(fasta_fname.c_str());

	if (!inf.is_open()) {
		throw runtime_error("Input file could not be read");
	}

	if (inf.peek() != '>') {
		throw runtime_error("First line of the fasta file must be the header");
	}

	vector<char> window(window_size);

	size_t p;  // genomic track index
	size_t k;  // window index
	bool in_progress = false;

	while (!inf.eof()) {

		if (inf.peek() == '>') {

			if (in_progress) {
				// finish scan for previous window
				// temporarily shrink window s.t. no extra nucleotides are counted
				window.resize(k);
				tracks.back().push_back(p+1, get_gc_content_of_window(window));
				window.resize(window_size);
				in_progress = false;
			}

			// start new track

			tracks.resize(tracks.size() + 1);

			// read header line
			
			string line;
			getline(inf, line);
			tracks.back().name = line.substr(1);

			// reset indices
			p = 0;
			k = 0;

		} else {

			// scan sequences

			// initialize line here s.t. if getline fails, line will be empty
			string line;
			getline(inf, line);
			if (line.empty()) continue;

			for (size_t i = 0; i < line.length(); ++i) {
				// extract next nucleotide, ignoring everything else
				char w = line[i];
				if (!isalpha(w)) continue;
				w = toupper(w);
				if (!is_nucleotide(w)) continue;

				window[k++] = w;
				in_progress = true;

				if (k == window_size) {
					// calculate GC content of window and store results
					
					// add data point, converting from 0-indexing to 1-indexing
					tracks.back().push_back(p+1, get_gc_content_of_window(window));

					// shift window by step_size and reuse nucleotides in overlap
					vector<char>::iterator begin = window.begin();
					copy(begin + step_size, window.end(), begin);
					p += step_size;
					k = window_size - step_size;
					in_progress = false;
				}
			}

		}

	}

	// finish scan for final window, if not already done
	if (in_progress) {
		// temporarily shrink window s.t. no extra nucleotides are counted
		window.resize(k);
		tracks.back().push_back(p+1, get_gc_content_of_window(window));
		window.resize(window_size);
		in_progress = false;
	}

}

}
