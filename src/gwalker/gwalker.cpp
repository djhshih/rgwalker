#include <iostream>
#include <string>

#include "gc_content.hpp"

using namespace std;

int main(int argc, char* argv[]) {

	if (argc-1 < 1) {
		cerr << "usage: " << argv[0] << " <input.fasta>" << endl;
		return 1;
	}

	string fname = argv[1];

	vector<gwalker::genomic_track> tracks;
	string seq_name;

	gwalker::scan_gc_content(fname, 100, 50, tracks);

	const char sep = '\t';
	for (size_t i = 0; i < tracks.size(); ++i) {
		const gwalker::genomic_track& track = tracks[i];
		const string& name = track.name;
		for (size_t j = 0; j < track.positions.size(); ++j) {
			cout << name << sep << track.positions[j] << sep << track.values[j] << endl;
		}
	}

	return 0;
}
