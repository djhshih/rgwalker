#ifndef _gc_content_hpp_
#define _gc_content_hpp_


#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <stdexcept>
#include <algorithm>


namespace gwalker {

using namespace std;

typedef long unsigned int position_t;


struct genomic_track {
	string name;
	vector<position_t> positions;
	vector<double> values;

	void push_back(position_t position, double value) {
		positions.push_back(position);
		values.push_back(value);
	}

	size_t size() const {
		return positions.size();
	}
};

void scan_gc_content(
	const string& fasta_fname, long int window_size, long int step_size,
	vector<genomic_track>& tracks
);

}

#endif
