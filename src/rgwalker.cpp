#include <Rcpp.h>
using namespace Rcpp;

#include "gwalker/gc_content.cpp"

//' Scan GC content
//'
//' This function computes GC content of a fasta file.
//'
//' @param fasta_fname   input fasta file name
//' @param window_size   size of each window (bp)
//' @param step_size     step by which to shift window
//'                      \code{(step.size < window.size)}
//' @export
// [[Rcpp::export]]
List scan_gc_content(
	const std::string& fasta_fname, long int window_size, long int step_size
) {

	std::vector<gwalker::genomic_track> tracks;

	gwalker::scan_gc_content(fasta_fname, window_size, step_size, tracks);

	List output;
	std::vector<gwalker::genomic_track>::const_iterator
		it = tracks.begin(), end = tracks.end();
	for (; it != end; ++it) {
		output[it->name] = List::create(
			_["positions"] = it->positions,
			_["values"] = it->values
		);
	}

	return output;
}
